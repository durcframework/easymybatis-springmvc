package com.myapp.dao;

import net.oschina.durcframework.easymybatis.dao.CrudDao;
import com.myapp.entity.UserInfo;

public interface UserInfoDao extends CrudDao<UserInfo> {
}