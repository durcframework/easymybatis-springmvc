package com.myapp.common.msg;

public interface MsgInfo<T> {
	String getMsg();
	T getCode();
}
